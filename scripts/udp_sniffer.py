#!/usr/bin/env python
import rospy
import socket as sk
import time
import select
from std_msgs.msg import Float32

class UdpSniffer(object):
    def __init__(self, dst_host, dst_port, src_host=None, buf = 8096):
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = dst_host
        self.port = dst_port
        self.buf = buf
        self.src_host = src_host
        self.last_receive_time = time.time()
        self.last_recover_time = time.time()
        self.over_threshold = False
        self.UDPSock.bind((self.host,self.port))
        self.pub = rospy.Publisher("/udp_droptime",Float32,queue_size=1)

        self.over_threshold = False

    def __del__(self):
        self.UDPSock.close()
    
    def _ready_to_read(self):
        # Return True if the socket is ready to read (still has msg in queue), False if not.
        socketlist = [self.UDPSock]
        readReadyList, writeReadyList, errorList = select.select(socketlist,[],[],0.0)
        if len(readReadyList)==0:
            return False
        else:
            return True

    def received_msg(self):
        if not self._ready_to_read():
            return False
        else:
            if self.src_host is None:
                # Receive at least a msg from any source
                return True
            else:
                # Go through the msg queue and check if any is from the correct source
                while self._ready_to_read():
                    msg,src = self.UDPSock.recvfrom(self.buf)
                    if src[0] == self.src_host:
                        # Receive a msg from the correct source
                        # self.clear_udp_queue()
                        return True
                # Receive msg, but none from correct soruce
                return False

    def clear_udp_queue(self):
        while self._ready_to_read():
            msg,src = self.UDPSock.recvfrom(self.buf)   

    def cb_tick(self,timer_event):
        # print "=========="
        current_time = time.time()
        since_last = current_time - self.last_receive_time
        print "[%.5f] Time since last: %.5f" %(current_time,since_last),

        self.pub.publish(Float32(since_last))

        if self.received_msg():
            if self.over_threshold:
                # TODO: log data
                print "\r[%.5f] == %.5f secs drop out, %.5f from last recover == " %(current_time,since_last,self.last_receive_time - self.last_recover_time)
                self.last_recover_time = current_time
                self.over_threshold = False
            else:
                print "\r",

            self.last_receive_time = time.time()
            self.clear_udp_queue()
        else:    
            if since_last > 0.1:
                self.over_threshold = True
            print "\r",

    def save_data(self):
        print ""
        print "Saving data!"
        # TODO: Save data

if __name__ == '__main__':
    rospy.init_node('udp_sniffer', anonymous=False)    
    sniffer = UdpSniffer("192.168.0.56",30056,"192.168.0.19")
    tick_timer = rospy.Timer(rospy.Duration.from_sec(0.002),sniffer.cb_tick)
    rospy.on_shutdown(sniffer.save_data)
    rospy.spin()
