#!/usr/bin/env python
import rospy
import time
import socket as sk
import serial
from std_msgs.msg import Float32
# TODO implement CommLog message
# TODO implement rosbag logging

class UdpSender(object):
    def __init__(self,host,port):
        # Initialize the comm with host and port
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = host
        self.port = port
        print '[comm_logger] Seding to host %s port %s' %(self.host,self.port)

    def __del__(self):
        self.UDPSock.close()

    def send(self,msg):      
        # Deliver through UDP to the destination
        if(self.UDPSock.sendto("\t%s\n" %msg,(self.host,self.port))):
            pass

    def cb_timer(self,timerEvent):
        self.send("%s" %time.time())


class SerialSniffer(object):
    def __init__(self, device, braud):
        self.ser = serial.Serial(device,braud,timeout=0)
        self.ser.flushInput()
        self.ser.flushOutput()
        self.last_receive_time = time.time()
        self.last_recover_time = time.time()
        self.over_threshold = False
        self.pub = rospy.Publisher("/droptime",Float32,queue_size=1)

        self.buf = ""
        # TODO implement printouts

    def __del__(self):
        pass

    def received_msg(self):
        bytesToRead = self.ser.inWaiting()
        if bytesToRead > 0:
            data = self.ser.read(bytesToRead)
            # data = self.ser.readline()
            print data
            # self.ser.flushInput()
            return True
        else:
            return False 

    def cb_tick(self,timer_event):
        # print "=========="
        current_time = time.time()
        since_last = current_time - self.last_receive_time
        print "[%.5f] Time since last: %.5f" %(current_time,since_last),

        self.pub.publish(Float32(since_last))
        if self.received_msg():
            if self.over_threshold:
                # TODO: log data
                print "\r[%.5f] == %.5f secs drop out, %.5f from last recover == " %(current_time,since_last,self.last_receive_time - self.last_recover_time)
                self.last_recover_time = current_time
                self.over_threshold = False
            else:
                print "\r",

            self.last_receive_time = time.time()
            # self.clear_udp_queue()
        else:    
            if since_last > 0.1:
                self.over_threshold = True
            print "\r",

    def on_shutdown(self):
        pass
        # TODO save data

if __name__ == '__main__':
    # TODO take input arguments
    host = '192.168.0.55'
    port = 30055
    rospy.init_node('comm_logger', anonymous=False)
    udp_sender = UdpSender(host,port)
    sniffer = SerialSniffer('/dev/ttyUSB0',230400)
    
    send_timer = rospy.Timer(rospy.Duration(0.01), udp_sender.cb_timer)
    recv_timer = rospy.Timer(rospy.Duration.from_sec(0.002),sniffer.cb_tick)

    rospy.on_shutdown(sniffer.on_shutdown)
    rospy.spin()