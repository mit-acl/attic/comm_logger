#!/usr/bin/env python
import rospy
import serial
import time
import rospkg
import rosbag
import datetime
from comm_logger.msg import CommLog

class SerialLogger(object):
    def __init__(self,device,braud):
        self.ser = serial.Serial(device,braud,timeout=0)
        self.buf = ""
        self.pharsed_value = None
        self.last_recv_time = time.time()

        # Setup the bag
        self.write_to_bag = False
        self.pkg_path = rospkg.RosPack().get_path('comm_logger')+"/log/"

        self.bag_start_time = None
        self.bag = None
        self.setup_new_bag()

    def pharse_buffer(self):
        if len(self.buf) == 16:
            # print "Got it: %.5f" %(float(self.buf))
            try:
                self.pharsed_value = float(self.buf)
            except ValueError:
                self.pharsed_value = None
        else:
            self.pharsed_value = None

    def cb_tick(self,timer_event):
        current_time = time.time()
        bytesToRead = self.ser.inWaiting()
        data = self.ser.read(bytesToRead)

        commLogMsg = CommLog()
        commLogMsg.current_time = current_time
        commLogMsg.last_recv_time = self.last_recv_time
        
        if "\n" in data:
            # split is at least length 2
            data_split = data.split("\n")
            self.buf = self.buf + data_split[-2]
            self.pharse_buffer()
            self.buf = data_split[-1]
        else:
            # no end of line in data, append to buffer
            self.buf = self.buf + data
            self.pharse_buffer()

        if self.pharsed_value is not None:
            if self.write_to_bag:
                print "\r[%.5f]:%.5f Latency:%.5f Last recv: %.5f ago"%(current_time,self.pharsed_value,
                    current_time - self.pharsed_value,
                    current_time - self.last_recv_time),

            commLogMsg.receive_data = True
            commLogMsg.latency = current_time - self.pharsed_value

            self.last_recv_time = current_time
        else:
            if self.write_to_bag:
                print "\r[%.5f]:%s"%(current_time,self.pharsed_value),
            commLogMsg.receive_data = False

        # Write to bag
        if self.write_to_bag:
            self.bag.write('comm_log',commLogMsg)

        # Switch to next bag if time passed
        if current_time - self.bag_start_time >= 300.0:
            self.close_bag()
            self.setup_new_bag()

    def close_bag(self):
        print "\n[%.5f]:Closing bag"%(time.time())
        self.write_to_bag = False #This prevents the current bag from being written during closing.
        self.bag.close()

    def setup_new_bag(self):
        datetime_now = datetime.datetime.now()
        bag_name = datetime_now.strftime("%Y-%m-%d-%H-%M-%S") + ".bag"
        print "\n[%.5f]:Starting bag %s" %(time.time(),bag_name)
        self.bag = rosbag.Bag(self.pkg_path+bag_name,'w')
        self.write_to_bag = True
        self.bag_start_time = time.time()

    def cb_bag_switch(self,timer_event):
        # Close the current bag and open a new one
        self.close_bag()
        self.setup_new_bag()

    def cb_on_shutdown(self):
        self.close_bag()

if __name__ == '__main__':
    # TODO take input arguments
    rospy.init_node('comm_logger_recv', anonymous=False)
    # self.ser = serial.Serial(device,braud,timeout=0)
    serial_logger = SerialLogger('/dev/ttyUSB0',230400)
    recv_timer = rospy.Timer(rospy.Duration.from_sec(0.005),serial_logger.cb_tick)
    # bag_switch_timer = rospy.Timer(rospy.Duration.from_sec(120.0),serial_logger.cb_bag_switch)
    rospy.on_shutdown(serial_logger.cb_on_shutdown)
    rospy.spin()
