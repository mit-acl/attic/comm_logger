#!/usr/bin/env python
import rospkg
import rosbag
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import sys

# Load the bag
rospack = rospkg.RosPack()
pkg_path = rospack.get_path('comm_logger')+"/log/"
bag_file_name = sys.argv[1]
bag = rosbag.Bag(pkg_path+bag_file_name)

# Read from the bag and construct data dictionary
data_dict = defaultdict(list)
for topic, msg, t in bag.read_messages():
    data_dict['current_time'].append(msg.current_time)
    data_dict['last_recv_time'].append(msg.last_recv_time)
    data_dict['receive_data'].append(msg.receive_data)
    data_dict['latency'].append(msg.latency)

# Construct DataFrame
df = pd.DataFrame(data_dict)
df_group = df.groupby('receive_data')
df_recv = df_group.get_group(True).copy()
df_recv['since_last'] = df_recv.current_time - df_recv.last_recv_time


# # Plot since_last_receive time
plt.figure()
plt.plot(df_recv.current_time,df_recv.since_last)
plt.xlabel("Time (sec)")
plt.ylabel("Time between receives (sec)")
plt.title("Dropout time")

plt.figure()
# Plot seperation histogram
plt.hist(df_recv.since_last.tolist(),bins=50)
plt.xlabel("Since last packet (sec)")
plt.ylabel("Number of times")
plt.title("Dropout time histogram")

# Plot latency
plt.figure()
plt.hist(df_recv.latency.tolist(),bins=50)
plt.xlabel("Latency (sec)")
plt.ylabel("Number of times")
plt.title("Latency")

plt.show()
