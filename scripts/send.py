#!/usr/bin/env python
import rospy
import time
import socket as sk
# TODO implement CommLog message
# TODO implement rosbag logging
from std_msgs.msg import Float32
# from comm_logger.msg import CommLog

class UdpSender(object):
    def __init__(self,host,port):
        # Initialize the comm with host and port
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = host
        self.port = port
        print '[comm_logger] Seding to host %s port %s' %(self.host,self.port)

    def __del__(self):
        self.UDPSock.close()

    def send(self,msg):      
        # Deliver through UDP to the destination
        if(self.UDPSock.sendto("%s\n" %msg,(self.host,self.port))):
            pass

    def cb_timer(self,timerEvent):
        self.send("%.5f"%time.time())


if __name__ == '__main__':
    # TODO take input arguments
    host = '192.168.0.55'
    port = 30055
    rospy.init_node('comm_logger_send', anonymous=False)
    udp_sender = UdpSender(host,port)    
    send_timer = rospy.Timer(rospy.Duration(0.01), udp_sender.cb_timer)
    rospy.spin()